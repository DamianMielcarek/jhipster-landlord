import { ICharge } from 'app/shared/model/charge.model';
import { IFlat } from 'app/shared/model/flat.model';

export interface ITenant {
  id?: number;
  firstName?: string;
  lastName?: string;
  pesel?: string;
  charges?: ICharge[];
  flat?: IFlat;
}

export class Tenant implements ITenant {
  constructor(
    public id?: number,
    public firstName?: string,
    public lastName?: string,
    public pesel?: string,
    public charges?: ICharge[],
    public flat?: IFlat
  ) {}
}
