import { IFlat } from 'app/shared/model/flat.model';

export interface IInvoice {
  id?: number;
  amount?: number;
  flats?: IFlat[];
}

export class Invoice implements IInvoice {
  constructor(public id?: number, public amount?: number, public flats?: IFlat[]) {}
}
