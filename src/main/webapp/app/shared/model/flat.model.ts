import { ITenant } from 'app/shared/model/tenant.model';
import { IInvoice } from 'app/shared/model/invoice.model';

export interface IFlat {
  id?: number;
  address?: string;
  numberOfRooms?: number;
  floor?: number;
  tenants?: ITenant[];
  invoices?: IInvoice[];
}

export class Flat implements IFlat {
  constructor(
    public id?: number,
    public address?: string,
    public numberOfRooms?: number,
    public floor?: number,
    public tenants?: ITenant[],
    public invoices?: IInvoice[]
  ) {}
}
