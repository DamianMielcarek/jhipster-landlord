import { ITenant } from 'app/shared/model/tenant.model';

export interface ICharge {
  id?: number;
  water?: number;
  electricity?: number;
  gas?: number;
  garbage?: number;
  tenant?: ITenant;
}

export class Charge implements ICharge {
  constructor(
    public id?: number,
    public water?: number,
    public electricity?: number,
    public gas?: number,
    public garbage?: number,
    public tenant?: ITenant
  ) {}
}
