import { NgModule } from '@angular/core';

import { JhipsterLandlordSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
  imports: [JhipsterLandlordSharedLibsModule],
  declarations: [JhiAlertComponent, JhiAlertErrorComponent],
  exports: [JhipsterLandlordSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class JhipsterLandlordSharedCommonModule {}
