import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { JhipsterLandlordSharedModule } from 'app/shared';
import {
  ChargeComponent,
  ChargeDetailComponent,
  ChargeUpdateComponent,
  ChargeDeletePopupComponent,
  ChargeDeleteDialogComponent,
  chargeRoute,
  chargePopupRoute
} from './';

const ENTITY_STATES = [...chargeRoute, ...chargePopupRoute];

@NgModule({
  imports: [JhipsterLandlordSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [ChargeComponent, ChargeDetailComponent, ChargeUpdateComponent, ChargeDeleteDialogComponent, ChargeDeletePopupComponent],
  entryComponents: [ChargeComponent, ChargeUpdateComponent, ChargeDeleteDialogComponent, ChargeDeletePopupComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JhipsterLandlordChargeModule {}
