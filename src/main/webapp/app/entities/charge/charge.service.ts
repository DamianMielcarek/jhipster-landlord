import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ICharge } from 'app/shared/model/charge.model';

type EntityResponseType = HttpResponse<ICharge>;
type EntityArrayResponseType = HttpResponse<ICharge[]>;

@Injectable({ providedIn: 'root' })
export class ChargeService {
  public resourceUrl = SERVER_API_URL + 'api/charges';

  constructor(protected http: HttpClient) {}

  create(charge: ICharge): Observable<EntityResponseType> {
    return this.http.post<ICharge>(this.resourceUrl, charge, { observe: 'response' });
  }

  update(charge: ICharge): Observable<EntityResponseType> {
    return this.http.put<ICharge>(this.resourceUrl, charge, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ICharge>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ICharge[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
