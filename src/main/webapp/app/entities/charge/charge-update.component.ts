import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { ICharge, Charge } from 'app/shared/model/charge.model';
import { ChargeService } from './charge.service';
import { ITenant } from 'app/shared/model/tenant.model';
import { TenantService } from 'app/entities/tenant';

@Component({
  selector: 'jhi-charge-update',
  templateUrl: './charge-update.component.html'
})
export class ChargeUpdateComponent implements OnInit {
  charge: ICharge;
  isSaving: boolean;

  tenants: ITenant[];

  editForm = this.fb.group({
    id: [],
    water: [null, [Validators.required]],
    electricity: [null, [Validators.required]],
    gas: [null, [Validators.required]],
    garbage: [null, [Validators.required]],
    tenant: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected chargeService: ChargeService,
    protected tenantService: TenantService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ charge }) => {
      this.updateForm(charge);
      this.charge = charge;
    });
    this.tenantService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<ITenant[]>) => mayBeOk.ok),
        map((response: HttpResponse<ITenant[]>) => response.body)
      )
      .subscribe((res: ITenant[]) => (this.tenants = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(charge: ICharge) {
    this.editForm.patchValue({
      id: charge.id,
      water: charge.water,
      electricity: charge.electricity,
      gas: charge.gas,
      garbage: charge.garbage,
      tenant: charge.tenant
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const charge = this.createFromForm();
    if (charge.id !== undefined) {
      this.subscribeToSaveResponse(this.chargeService.update(charge));
    } else {
      this.subscribeToSaveResponse(this.chargeService.create(charge));
    }
  }

  private createFromForm(): ICharge {
    const entity = {
      ...new Charge(),
      id: this.editForm.get(['id']).value,
      water: this.editForm.get(['water']).value,
      electricity: this.editForm.get(['electricity']).value,
      gas: this.editForm.get(['gas']).value,
      garbage: this.editForm.get(['garbage']).value,
      tenant: this.editForm.get(['tenant']).value
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICharge>>) {
    result.subscribe((res: HttpResponse<ICharge>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackTenantById(index: number, item: ITenant) {
    return item.id;
  }
}
