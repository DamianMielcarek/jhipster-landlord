import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ICharge } from 'app/shared/model/charge.model';
import { AccountService } from 'app/core';
import { ChargeService } from './charge.service';

@Component({
  selector: 'jhi-charge',
  templateUrl: './charge.component.html'
})
export class ChargeComponent implements OnInit, OnDestroy {
  charges: ICharge[];
  currentAccount: any;
  eventSubscriber: Subscription;

  constructor(
    protected chargeService: ChargeService,
    protected jhiAlertService: JhiAlertService,
    protected eventManager: JhiEventManager,
    protected accountService: AccountService
  ) {}

  loadAll() {
    this.chargeService
      .query()
      .pipe(
        filter((res: HttpResponse<ICharge[]>) => res.ok),
        map((res: HttpResponse<ICharge[]>) => res.body)
      )
      .subscribe(
        (res: ICharge[]) => {
          this.charges = res;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInCharges();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: ICharge) {
    return item.id;
  }

  registerChangeInCharges() {
    this.eventSubscriber = this.eventManager.subscribe('chargeListModification', response => this.loadAll());
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
