import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { ITenant, Tenant } from 'app/shared/model/tenant.model';
import { TenantService } from './tenant.service';
import { IFlat } from 'app/shared/model/flat.model';
import { FlatService } from 'app/entities/flat';

@Component({
  selector: 'jhi-tenant-update',
  templateUrl: './tenant-update.component.html'
})
export class TenantUpdateComponent implements OnInit {
  tenant: ITenant;
  isSaving: boolean;

  flats: IFlat[];

  editForm = this.fb.group({
    id: [],
    firstName: [null, [Validators.required]],
    lastName: [null, [Validators.required]],
    pesel: [null, [Validators.required]],
    flat: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected tenantService: TenantService,
    protected flatService: FlatService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ tenant }) => {
      this.updateForm(tenant);
      this.tenant = tenant;
    });
    this.flatService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IFlat[]>) => mayBeOk.ok),
        map((response: HttpResponse<IFlat[]>) => response.body)
      )
      .subscribe((res: IFlat[]) => (this.flats = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(tenant: ITenant) {
    this.editForm.patchValue({
      id: tenant.id,
      firstName: tenant.firstName,
      lastName: tenant.lastName,
      pesel: tenant.pesel,
      flat: tenant.flat
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const tenant = this.createFromForm();
    if (tenant.id !== undefined) {
      this.subscribeToSaveResponse(this.tenantService.update(tenant));
    } else {
      this.subscribeToSaveResponse(this.tenantService.create(tenant));
    }
  }

  private createFromForm(): ITenant {
    const entity = {
      ...new Tenant(),
      id: this.editForm.get(['id']).value,
      firstName: this.editForm.get(['firstName']).value,
      lastName: this.editForm.get(['lastName']).value,
      pesel: this.editForm.get(['pesel']).value,
      flat: this.editForm.get(['flat']).value
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITenant>>) {
    result.subscribe((res: HttpResponse<ITenant>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackFlatById(index: number, item: IFlat) {
    return item.id;
  }
}
