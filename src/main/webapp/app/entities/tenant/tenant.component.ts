import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ITenant } from 'app/shared/model/tenant.model';
import { AccountService } from 'app/core';
import { TenantService } from './tenant.service';

@Component({
  selector: 'jhi-tenant',
  templateUrl: './tenant.component.html'
})
export class TenantComponent implements OnInit, OnDestroy {
  tenants: ITenant[];
  currentAccount: any;
  eventSubscriber: Subscription;

  constructor(
    protected tenantService: TenantService,
    protected jhiAlertService: JhiAlertService,
    protected eventManager: JhiEventManager,
    protected accountService: AccountService
  ) {}

  loadAll() {
    this.tenantService
      .query()
      .pipe(
        filter((res: HttpResponse<ITenant[]>) => res.ok),
        map((res: HttpResponse<ITenant[]>) => res.body)
      )
      .subscribe(
        (res: ITenant[]) => {
          this.tenants = res;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInTenants();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: ITenant) {
    return item.id;
  }

  registerChangeInTenants() {
    this.eventSubscriber = this.eventManager.subscribe('tenantListModification', response => this.loadAll());
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
