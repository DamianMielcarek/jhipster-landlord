import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { JhipsterLandlordSharedModule } from 'app/shared';
import {
  FlatComponent,
  FlatDetailComponent,
  FlatUpdateComponent,
  FlatDeletePopupComponent,
  FlatDeleteDialogComponent,
  flatRoute,
  flatPopupRoute
} from './';

const ENTITY_STATES = [...flatRoute, ...flatPopupRoute];

@NgModule({
  imports: [JhipsterLandlordSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [FlatComponent, FlatDetailComponent, FlatUpdateComponent, FlatDeleteDialogComponent, FlatDeletePopupComponent],
  entryComponents: [FlatComponent, FlatUpdateComponent, FlatDeleteDialogComponent, FlatDeletePopupComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JhipsterLandlordFlatModule {}
