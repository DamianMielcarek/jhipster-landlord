import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Flat } from 'app/shared/model/flat.model';
import { FlatService } from './flat.service';
import { FlatComponent } from './flat.component';
import { FlatDetailComponent } from './flat-detail.component';
import { FlatUpdateComponent } from './flat-update.component';
import { FlatDeletePopupComponent } from './flat-delete-dialog.component';
import { IFlat } from 'app/shared/model/flat.model';

@Injectable({ providedIn: 'root' })
export class FlatResolve implements Resolve<IFlat> {
  constructor(private service: FlatService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IFlat> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Flat>) => response.ok),
        map((flat: HttpResponse<Flat>) => flat.body)
      );
    }
    return of(new Flat());
  }
}

export const flatRoute: Routes = [
  {
    path: '',
    component: FlatComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Flats'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: FlatDetailComponent,
    resolve: {
      flat: FlatResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Flats'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: FlatUpdateComponent,
    resolve: {
      flat: FlatResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Flats'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: FlatUpdateComponent,
    resolve: {
      flat: FlatResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Flats'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const flatPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: FlatDeletePopupComponent,
    resolve: {
      flat: FlatResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Flats'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
