export * from './flat.service';
export * from './flat-update.component';
export * from './flat-delete-dialog.component';
export * from './flat-detail.component';
export * from './flat.component';
export * from './flat.route';
