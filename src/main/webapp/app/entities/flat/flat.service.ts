import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IFlat } from 'app/shared/model/flat.model';

type EntityResponseType = HttpResponse<IFlat>;
type EntityArrayResponseType = HttpResponse<IFlat[]>;

@Injectable({ providedIn: 'root' })
export class FlatService {
  public resourceUrl = SERVER_API_URL + 'api/flats';

  constructor(protected http: HttpClient) {}

  create(flat: IFlat): Observable<EntityResponseType> {
    return this.http.post<IFlat>(this.resourceUrl, flat, { observe: 'response' });
  }

  update(flat: IFlat): Observable<EntityResponseType> {
    return this.http.put<IFlat>(this.resourceUrl, flat, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IFlat>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IFlat[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
