import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IFlat, Flat } from 'app/shared/model/flat.model';
import { FlatService } from './flat.service';
import { IInvoice } from 'app/shared/model/invoice.model';
import { InvoiceService } from 'app/entities/invoice';

@Component({
  selector: 'jhi-flat-update',
  templateUrl: './flat-update.component.html'
})
export class FlatUpdateComponent implements OnInit {
  flat: IFlat;
  isSaving: boolean;

  invoices: IInvoice[];

  editForm = this.fb.group({
    id: [],
    address: [null, [Validators.required]],
    numberOfRooms: [null, [Validators.required]],
    floor: [null, [Validators.required]],
    invoices: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected flatService: FlatService,
    protected invoiceService: InvoiceService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ flat }) => {
      this.updateForm(flat);
      this.flat = flat;
    });
    this.invoiceService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IInvoice[]>) => mayBeOk.ok),
        map((response: HttpResponse<IInvoice[]>) => response.body)
      )
      .subscribe((res: IInvoice[]) => (this.invoices = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(flat: IFlat) {
    this.editForm.patchValue({
      id: flat.id,
      address: flat.address,
      numberOfRooms: flat.numberOfRooms,
      floor: flat.floor,
      invoices: flat.invoices
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const flat = this.createFromForm();
    if (flat.id !== undefined) {
      this.subscribeToSaveResponse(this.flatService.update(flat));
    } else {
      this.subscribeToSaveResponse(this.flatService.create(flat));
    }
  }

  private createFromForm(): IFlat {
    const entity = {
      ...new Flat(),
      id: this.editForm.get(['id']).value,
      address: this.editForm.get(['address']).value,
      numberOfRooms: this.editForm.get(['numberOfRooms']).value,
      floor: this.editForm.get(['floor']).value,
      invoices: this.editForm.get(['invoices']).value
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IFlat>>) {
    result.subscribe((res: HttpResponse<IFlat>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackInvoiceById(index: number, item: IInvoice) {
    return item.id;
  }

  getSelected(selectedVals: Array<any>, option: any) {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}
