import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IFlat } from 'app/shared/model/flat.model';

@Component({
  selector: 'jhi-flat-detail',
  templateUrl: './flat-detail.component.html'
})
export class FlatDetailComponent implements OnInit {
  flat: IFlat;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ flat }) => {
      this.flat = flat;
    });
  }

  previousState() {
    window.history.back();
  }
}
