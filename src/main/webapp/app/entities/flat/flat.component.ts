import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IFlat } from 'app/shared/model/flat.model';
import { AccountService } from 'app/core';
import { FlatService } from './flat.service';

@Component({
  selector: 'jhi-flat',
  templateUrl: './flat.component.html'
})
export class FlatComponent implements OnInit, OnDestroy {
  flats: IFlat[];
  currentAccount: any;
  eventSubscriber: Subscription;

  constructor(
    protected flatService: FlatService,
    protected jhiAlertService: JhiAlertService,
    protected eventManager: JhiEventManager,
    protected accountService: AccountService
  ) {}

  loadAll() {
    this.flatService
      .query()
      .pipe(
        filter((res: HttpResponse<IFlat[]>) => res.ok),
        map((res: HttpResponse<IFlat[]>) => res.body)
      )
      .subscribe(
        (res: IFlat[]) => {
          this.flats = res;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInFlats();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IFlat) {
    return item.id;
  }

  registerChangeInFlats() {
    this.eventSubscriber = this.eventManager.subscribe('flatListModification', response => this.loadAll());
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
