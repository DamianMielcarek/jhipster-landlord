import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IFlat } from 'app/shared/model/flat.model';
import { FlatService } from './flat.service';

@Component({
  selector: 'jhi-flat-delete-dialog',
  templateUrl: './flat-delete-dialog.component.html'
})
export class FlatDeleteDialogComponent {
  flat: IFlat;

  constructor(protected flatService: FlatService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.flatService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'flatListModification',
        content: 'Deleted an flat'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-flat-delete-popup',
  template: ''
})
export class FlatDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ flat }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(FlatDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.flat = flat;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/flat', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/flat', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
