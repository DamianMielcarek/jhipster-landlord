package com.jhipster.landlord.service;

import com.jhipster.landlord.domain.Flat;
import com.jhipster.landlord.repository.FlatRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Flat}.
 */
@Service
@Transactional
public class FlatService {

    private final Logger log = LoggerFactory.getLogger(FlatService.class);

    private final FlatRepository flatRepository;

    public FlatService(FlatRepository flatRepository) {
        this.flatRepository = flatRepository;
    }

    /**
     * Save a flat.
     *
     * @param flat the entity to save.
     * @return the persisted entity.
     */
    public Flat save(Flat flat) {
        log.debug("Request to save Flat : {}", flat);
        return flatRepository.save(flat);
    }

    /**
     * Get all the flats.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<Flat> findAll() {
        log.debug("Request to get all Flats");
        return flatRepository.findAllWithEagerRelationships();
    }

    /**
     * Get all the flats with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    public Page<Flat> findAllWithEagerRelationships(Pageable pageable) {
        return flatRepository.findAllWithEagerRelationships(pageable);
    }
    

    /**
     * Get one flat by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Flat> findOne(Long id) {
        log.debug("Request to get Flat : {}", id);
        return flatRepository.findOneWithEagerRelationships(id);
    }

    /**
     * Delete the flat by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Flat : {}", id);
        flatRepository.deleteById(id);
    }
}
