package com.jhipster.landlord.service;

import com.jhipster.landlord.domain.Charge;
import com.jhipster.landlord.repository.ChargeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Charge}.
 */
@Service
@Transactional
public class ChargeService {

    private final Logger log = LoggerFactory.getLogger(ChargeService.class);

    private final ChargeRepository chargeRepository;

    public ChargeService(ChargeRepository chargeRepository) {
        this.chargeRepository = chargeRepository;
    }

    /**
     * Save a charge.
     *
     * @param charge the entity to save.
     * @return the persisted entity.
     */
    public Charge save(Charge charge) {
        log.debug("Request to save Charge : {}", charge);
        return chargeRepository.save(charge);
    }

    /**
     * Get all the charges.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<Charge> findAll() {
        log.debug("Request to get all Charges");
        return chargeRepository.findAll();
    }


    /**
     * Get one charge by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Charge> findOne(Long id) {
        log.debug("Request to get Charge : {}", id);
        return chargeRepository.findById(id);
    }

    /**
     * Delete the charge by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Charge : {}", id);
        chargeRepository.deleteById(id);
    }
}
