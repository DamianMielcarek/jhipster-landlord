package com.jhipster.landlord.repository;

import com.jhipster.landlord.domain.Flat;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Flat entity.
 */
@Repository
public interface FlatRepository extends JpaRepository<Flat, Long> {

    @Query(value = "select distinct flat from Flat flat left join fetch flat.invoices",
        countQuery = "select count(distinct flat) from Flat flat")
    Page<Flat> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct flat from Flat flat left join fetch flat.invoices")
    List<Flat> findAllWithEagerRelationships();

    @Query("select flat from Flat flat left join fetch flat.invoices where flat.id =:id")
    Optional<Flat> findOneWithEagerRelationships(@Param("id") Long id);

}
