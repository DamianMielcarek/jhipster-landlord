package com.jhipster.landlord.web.rest;

import com.jhipster.landlord.domain.Flat;
import com.jhipster.landlord.service.FlatService;
import com.jhipster.landlord.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link Flat}.
 */
@RestController
@RequestMapping("/api")
public class FlatResource {

    private final Logger log = LoggerFactory.getLogger(FlatResource.class);

    private static final String ENTITY_NAME = "flat";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final FlatService flatService;

    public FlatResource(FlatService flatService) {
        this.flatService = flatService;
    }

    /**
     * {@code POST  /flats} : Create a new flat.
     *
     * @param flat the flat to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new flat, or with status {@code 400 (Bad Request)} if the flat has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/flats")
    public ResponseEntity<Flat> createFlat(@Valid @RequestBody Flat flat) throws URISyntaxException {
        log.debug("REST request to save Flat : {}", flat);
        if (flat.getId() != null) {
            throw new BadRequestAlertException("A new flat cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Flat result = flatService.save(flat);
        return ResponseEntity.created(new URI("/api/flats/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /flats} : Updates an existing flat.
     *
     * @param flat the flat to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated flat,
     * or with status {@code 400 (Bad Request)} if the flat is not valid,
     * or with status {@code 500 (Internal Server Error)} if the flat couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/flats")
    public ResponseEntity<Flat> updateFlat(@Valid @RequestBody Flat flat) throws URISyntaxException {
        log.debug("REST request to update Flat : {}", flat);
        if (flat.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Flat result = flatService.save(flat);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, flat.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /flats} : get all the flats.
     *
     * @param eagerload flag to eager load entities from relationships (This is applicable for many-to-many).
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of flats in body.
     */
    @GetMapping("/flats")
    public List<Flat> getAllFlats(@RequestParam(required = false, defaultValue = "false") boolean eagerload) {
        log.debug("REST request to get all Flats");
        return flatService.findAll();
    }

    /**
     * {@code GET  /flats/:id} : get the "id" flat.
     *
     * @param id the id of the flat to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the flat, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/flats/{id}")
    public ResponseEntity<Flat> getFlat(@PathVariable Long id) {
        log.debug("REST request to get Flat : {}", id);
        Optional<Flat> flat = flatService.findOne(id);
        return ResponseUtil.wrapOrNotFound(flat);
    }

    /**
     * {@code DELETE  /flats/:id} : delete the "id" flat.
     *
     * @param id the id of the flat to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/flats/{id}")
    public ResponseEntity<Void> deleteFlat(@PathVariable Long id) {
        log.debug("REST request to delete Flat : {}", id);
        flatService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
