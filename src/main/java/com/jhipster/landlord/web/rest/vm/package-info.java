/**
 * View Models used by Spring MVC REST controllers.
 */
package com.jhipster.landlord.web.rest.vm;
