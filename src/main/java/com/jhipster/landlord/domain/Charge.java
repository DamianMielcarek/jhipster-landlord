package com.jhipster.landlord.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * A Charge.
 */
@Entity
@Table(name = "charge")
public class Charge implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "water", precision = 21, scale = 2, nullable = false)
    private BigDecimal water;

    @NotNull
    @Column(name = "electricity", precision = 21, scale = 2, nullable = false)
    private BigDecimal electricity;

    @NotNull
    @Column(name = "gas", precision = 21, scale = 2, nullable = false)
    private BigDecimal gas;

    @NotNull
    @Column(name = "garbage", precision = 21, scale = 2, nullable = false)
    private BigDecimal garbage;

    @ManyToOne
    @JsonIgnoreProperties("charges")
    private Tenant tenant;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getWater() {
        return water;
    }

    public Charge water(BigDecimal water) {
        this.water = water;
        return this;
    }

    public void setWater(BigDecimal water) {
        this.water = water;
    }

    public BigDecimal getElectricity() {
        return electricity;
    }

    public Charge electricity(BigDecimal electricity) {
        this.electricity = electricity;
        return this;
    }

    public void setElectricity(BigDecimal electricity) {
        this.electricity = electricity;
    }

    public BigDecimal getGas() {
        return gas;
    }

    public Charge gas(BigDecimal gas) {
        this.gas = gas;
        return this;
    }

    public void setGas(BigDecimal gas) {
        this.gas = gas;
    }

    public BigDecimal getGarbage() {
        return garbage;
    }

    public Charge garbage(BigDecimal garbage) {
        this.garbage = garbage;
        return this;
    }

    public void setGarbage(BigDecimal garbage) {
        this.garbage = garbage;
    }

    public Tenant getTenant() {
        return tenant;
    }

    public Charge tenant(Tenant tenant) {
        this.tenant = tenant;
        return this;
    }

    public void setTenant(Tenant tenant) {
        this.tenant = tenant;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Charge)) {
            return false;
        }
        return id != null && id.equals(((Charge) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Charge{" +
            "id=" + getId() +
            ", water=" + getWater() +
            ", electricity=" + getElectricity() +
            ", gas=" + getGas() +
            ", garbage=" + getGarbage() +
            "}";
    }
}
