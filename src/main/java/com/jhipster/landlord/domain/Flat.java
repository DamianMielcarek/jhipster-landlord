package com.jhipster.landlord.domain;


import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Flat.
 */
@Entity
@Table(name = "flat")
public class Flat implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "address", nullable = false)
    private String address;

    @NotNull
    @Column(name = "number_of_rooms", nullable = false)
    private Integer numberOfRooms;

    @NotNull
    @Column(name = "floor", nullable = false)
    private Integer floor;

    @OneToMany(mappedBy = "flat")
    private Set<Tenant> tenants = new HashSet<>();

    @ManyToMany
    @JoinTable(name = "flat_invoice",
               joinColumns = @JoinColumn(name = "flat_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "invoice_id", referencedColumnName = "id"))
    private Set<Invoice> invoices = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public Flat address(String address) {
        this.address = address;
        return this;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getNumberOfRooms() {
        return numberOfRooms;
    }

    public Flat numberOfRooms(Integer numberOfRooms) {
        this.numberOfRooms = numberOfRooms;
        return this;
    }

    public void setNumberOfRooms(Integer numberOfRooms) {
        this.numberOfRooms = numberOfRooms;
    }

    public Integer getFloor() {
        return floor;
    }

    public Flat floor(Integer floor) {
        this.floor = floor;
        return this;
    }

    public void setFloor(Integer floor) {
        this.floor = floor;
    }

    public Set<Tenant> getTenants() {
        return tenants;
    }

    public Flat tenants(Set<Tenant> tenants) {
        this.tenants = tenants;
        return this;
    }

    public Flat addTenant(Tenant tenant) {
        this.tenants.add(tenant);
        tenant.setFlat(this);
        return this;
    }

    public Flat removeTenant(Tenant tenant) {
        this.tenants.remove(tenant);
        tenant.setFlat(null);
        return this;
    }

    public void setTenants(Set<Tenant> tenants) {
        this.tenants = tenants;
    }

    public Set<Invoice> getInvoices() {
        return invoices;
    }

    public Flat invoices(Set<Invoice> invoices) {
        this.invoices = invoices;
        return this;
    }

    public Flat addInvoice(Invoice invoice) {
        this.invoices.add(invoice);
        invoice.getFlats().add(this);
        return this;
    }

    public Flat removeInvoice(Invoice invoice) {
        this.invoices.remove(invoice);
        invoice.getFlats().remove(this);
        return this;
    }

    public void setInvoices(Set<Invoice> invoices) {
        this.invoices = invoices;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Flat)) {
            return false;
        }
        return id != null && id.equals(((Flat) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Flat{" +
            "id=" + getId() +
            ", address='" + getAddress() + "'" +
            ", numberOfRooms=" + getNumberOfRooms() +
            ", floor=" + getFloor() +
            "}";
    }
}
