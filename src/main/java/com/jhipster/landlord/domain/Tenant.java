package com.jhipster.landlord.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Tenant.
 */
@Entity
@Table(name = "tenant")
public class Tenant implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "first_name", nullable = false)
    private String firstName;

    @NotNull
    @Column(name = "last_name", nullable = false)
    private String lastName;

    @NotNull
    @Column(name = "pesel", nullable = false)
    private String pesel;

    @OneToMany(mappedBy = "tenant")
    private Set<Charge> charges = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("tenants")
    private Flat flat;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public Tenant firstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Tenant lastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPesel() {
        return pesel;
    }

    public Tenant pesel(String pesel) {
        this.pesel = pesel;
        return this;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    public Set<Charge> getCharges() {
        return charges;
    }

    public Tenant charges(Set<Charge> charges) {
        this.charges = charges;
        return this;
    }

    public Tenant addCharge(Charge charge) {
        this.charges.add(charge);
        charge.setTenant(this);
        return this;
    }

    public Tenant removeCharge(Charge charge) {
        this.charges.remove(charge);
        charge.setTenant(null);
        return this;
    }

    public void setCharges(Set<Charge> charges) {
        this.charges = charges;
    }

    public Flat getFlat() {
        return flat;
    }

    public Tenant flat(Flat flat) {
        this.flat = flat;
        return this;
    }

    public void setFlat(Flat flat) {
        this.flat = flat;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Tenant)) {
            return false;
        }
        return id != null && id.equals(((Tenant) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Tenant{" +
            "id=" + getId() +
            ", firstName='" + getFirstName() + "'" +
            ", lastName='" + getLastName() + "'" +
            ", pesel='" + getPesel() + "'" +
            "}";
    }
}
