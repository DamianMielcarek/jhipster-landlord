/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { JhipsterLandlordTestModule } from '../../../test.module';
import { FlatComponent } from 'app/entities/flat/flat.component';
import { FlatService } from 'app/entities/flat/flat.service';
import { Flat } from 'app/shared/model/flat.model';

describe('Component Tests', () => {
  describe('Flat Management Component', () => {
    let comp: FlatComponent;
    let fixture: ComponentFixture<FlatComponent>;
    let service: FlatService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [JhipsterLandlordTestModule],
        declarations: [FlatComponent],
        providers: []
      })
        .overrideTemplate(FlatComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(FlatComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(FlatService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Flat(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.flats[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
