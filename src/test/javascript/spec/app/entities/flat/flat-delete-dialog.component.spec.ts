/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { JhipsterLandlordTestModule } from '../../../test.module';
import { FlatDeleteDialogComponent } from 'app/entities/flat/flat-delete-dialog.component';
import { FlatService } from 'app/entities/flat/flat.service';

describe('Component Tests', () => {
  describe('Flat Management Delete Component', () => {
    let comp: FlatDeleteDialogComponent;
    let fixture: ComponentFixture<FlatDeleteDialogComponent>;
    let service: FlatService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [JhipsterLandlordTestModule],
        declarations: [FlatDeleteDialogComponent]
      })
        .overrideTemplate(FlatDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(FlatDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(FlatService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
