/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { JhipsterLandlordTestModule } from '../../../test.module';
import { FlatDetailComponent } from 'app/entities/flat/flat-detail.component';
import { Flat } from 'app/shared/model/flat.model';

describe('Component Tests', () => {
  describe('Flat Management Detail Component', () => {
    let comp: FlatDetailComponent;
    let fixture: ComponentFixture<FlatDetailComponent>;
    const route = ({ data: of({ flat: new Flat(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [JhipsterLandlordTestModule],
        declarations: [FlatDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(FlatDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(FlatDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.flat).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
