package com.jhipster.landlord.web.rest;

import com.jhipster.landlord.domain.Charge;
import com.jhipster.landlord.repository.ChargeRepository;
import com.jhipster.landlord.service.ChargeService;
import com.jhipster.landlord.JhipsterLandlordApp;
import com.jhipster.landlord.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.List;

import static com.jhipster.landlord.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link ChargeResource} REST controller.
 */
@SpringBootTest(classes = JhipsterLandlordApp.class)
public class ChargeResourceIT {

    private static final BigDecimal DEFAULT_WATER = new BigDecimal(1);
    private static final BigDecimal UPDATED_WATER = new BigDecimal(2);

    private static final BigDecimal DEFAULT_ELECTRICITY = new BigDecimal(1);
    private static final BigDecimal UPDATED_ELECTRICITY = new BigDecimal(2);

    private static final BigDecimal DEFAULT_GAS = new BigDecimal(1);
    private static final BigDecimal UPDATED_GAS = new BigDecimal(2);

    private static final BigDecimal DEFAULT_GARBAGE = new BigDecimal(1);
    private static final BigDecimal UPDATED_GARBAGE = new BigDecimal(2);

    @Autowired
    private ChargeRepository chargeRepository;

    @Autowired
    private ChargeService chargeService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restChargeMockMvc;

    private Charge charge;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ChargeResource chargeResource = new ChargeResource(chargeService);
        this.restChargeMockMvc = MockMvcBuilders.standaloneSetup(chargeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Charge createEntity(EntityManager em) {
        Charge charge = new Charge()
            .water(DEFAULT_WATER)
            .electricity(DEFAULT_ELECTRICITY)
            .gas(DEFAULT_GAS)
            .garbage(DEFAULT_GARBAGE);
        return charge;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Charge createUpdatedEntity(EntityManager em) {
        Charge charge = new Charge()
            .water(UPDATED_WATER)
            .electricity(UPDATED_ELECTRICITY)
            .gas(UPDATED_GAS)
            .garbage(UPDATED_GARBAGE);
        return charge;
    }

    @BeforeEach
    public void initTest() {
        charge = createEntity(em);
    }

    @Test
    @Transactional
    public void createCharge() throws Exception {
        int databaseSizeBeforeCreate = chargeRepository.findAll().size();

        // Create the Charge
        restChargeMockMvc.perform(post("/api/charges")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(charge)))
            .andExpect(status().isCreated());

        // Validate the Charge in the database
        List<Charge> chargeList = chargeRepository.findAll();
        assertThat(chargeList).hasSize(databaseSizeBeforeCreate + 1);
        Charge testCharge = chargeList.get(chargeList.size() - 1);
        assertThat(testCharge.getWater()).isEqualTo(DEFAULT_WATER);
        assertThat(testCharge.getElectricity()).isEqualTo(DEFAULT_ELECTRICITY);
        assertThat(testCharge.getGas()).isEqualTo(DEFAULT_GAS);
        assertThat(testCharge.getGarbage()).isEqualTo(DEFAULT_GARBAGE);
    }

    @Test
    @Transactional
    public void createChargeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = chargeRepository.findAll().size();

        // Create the Charge with an existing ID
        charge.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restChargeMockMvc.perform(post("/api/charges")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(charge)))
            .andExpect(status().isBadRequest());

        // Validate the Charge in the database
        List<Charge> chargeList = chargeRepository.findAll();
        assertThat(chargeList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkWaterIsRequired() throws Exception {
        int databaseSizeBeforeTest = chargeRepository.findAll().size();
        // set the field null
        charge.setWater(null);

        // Create the Charge, which fails.

        restChargeMockMvc.perform(post("/api/charges")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(charge)))
            .andExpect(status().isBadRequest());

        List<Charge> chargeList = chargeRepository.findAll();
        assertThat(chargeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkElectricityIsRequired() throws Exception {
        int databaseSizeBeforeTest = chargeRepository.findAll().size();
        // set the field null
        charge.setElectricity(null);

        // Create the Charge, which fails.

        restChargeMockMvc.perform(post("/api/charges")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(charge)))
            .andExpect(status().isBadRequest());

        List<Charge> chargeList = chargeRepository.findAll();
        assertThat(chargeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkGasIsRequired() throws Exception {
        int databaseSizeBeforeTest = chargeRepository.findAll().size();
        // set the field null
        charge.setGas(null);

        // Create the Charge, which fails.

        restChargeMockMvc.perform(post("/api/charges")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(charge)))
            .andExpect(status().isBadRequest());

        List<Charge> chargeList = chargeRepository.findAll();
        assertThat(chargeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkGarbageIsRequired() throws Exception {
        int databaseSizeBeforeTest = chargeRepository.findAll().size();
        // set the field null
        charge.setGarbage(null);

        // Create the Charge, which fails.

        restChargeMockMvc.perform(post("/api/charges")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(charge)))
            .andExpect(status().isBadRequest());

        List<Charge> chargeList = chargeRepository.findAll();
        assertThat(chargeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllCharges() throws Exception {
        // Initialize the database
        chargeRepository.saveAndFlush(charge);

        // Get all the chargeList
        restChargeMockMvc.perform(get("/api/charges?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(charge.getId().intValue())))
            .andExpect(jsonPath("$.[*].water").value(hasItem(DEFAULT_WATER.intValue())))
            .andExpect(jsonPath("$.[*].electricity").value(hasItem(DEFAULT_ELECTRICITY.intValue())))
            .andExpect(jsonPath("$.[*].gas").value(hasItem(DEFAULT_GAS.intValue())))
            .andExpect(jsonPath("$.[*].garbage").value(hasItem(DEFAULT_GARBAGE.intValue())));
    }
    
    @Test
    @Transactional
    public void getCharge() throws Exception {
        // Initialize the database
        chargeRepository.saveAndFlush(charge);

        // Get the charge
        restChargeMockMvc.perform(get("/api/charges/{id}", charge.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(charge.getId().intValue()))
            .andExpect(jsonPath("$.water").value(DEFAULT_WATER.intValue()))
            .andExpect(jsonPath("$.electricity").value(DEFAULT_ELECTRICITY.intValue()))
            .andExpect(jsonPath("$.gas").value(DEFAULT_GAS.intValue()))
            .andExpect(jsonPath("$.garbage").value(DEFAULT_GARBAGE.intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingCharge() throws Exception {
        // Get the charge
        restChargeMockMvc.perform(get("/api/charges/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCharge() throws Exception {
        // Initialize the database
        chargeService.save(charge);

        int databaseSizeBeforeUpdate = chargeRepository.findAll().size();

        // Update the charge
        Charge updatedCharge = chargeRepository.findById(charge.getId()).get();
        // Disconnect from session so that the updates on updatedCharge are not directly saved in db
        em.detach(updatedCharge);
        updatedCharge
            .water(UPDATED_WATER)
            .electricity(UPDATED_ELECTRICITY)
            .gas(UPDATED_GAS)
            .garbage(UPDATED_GARBAGE);

        restChargeMockMvc.perform(put("/api/charges")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedCharge)))
            .andExpect(status().isOk());

        // Validate the Charge in the database
        List<Charge> chargeList = chargeRepository.findAll();
        assertThat(chargeList).hasSize(databaseSizeBeforeUpdate);
        Charge testCharge = chargeList.get(chargeList.size() - 1);
        assertThat(testCharge.getWater()).isEqualTo(UPDATED_WATER);
        assertThat(testCharge.getElectricity()).isEqualTo(UPDATED_ELECTRICITY);
        assertThat(testCharge.getGas()).isEqualTo(UPDATED_GAS);
        assertThat(testCharge.getGarbage()).isEqualTo(UPDATED_GARBAGE);
    }

    @Test
    @Transactional
    public void updateNonExistingCharge() throws Exception {
        int databaseSizeBeforeUpdate = chargeRepository.findAll().size();

        // Create the Charge

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restChargeMockMvc.perform(put("/api/charges")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(charge)))
            .andExpect(status().isBadRequest());

        // Validate the Charge in the database
        List<Charge> chargeList = chargeRepository.findAll();
        assertThat(chargeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCharge() throws Exception {
        // Initialize the database
        chargeService.save(charge);

        int databaseSizeBeforeDelete = chargeRepository.findAll().size();

        // Delete the charge
        restChargeMockMvc.perform(delete("/api/charges/{id}", charge.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<Charge> chargeList = chargeRepository.findAll();
        assertThat(chargeList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Charge.class);
        Charge charge1 = new Charge();
        charge1.setId(1L);
        Charge charge2 = new Charge();
        charge2.setId(charge1.getId());
        assertThat(charge1).isEqualTo(charge2);
        charge2.setId(2L);
        assertThat(charge1).isNotEqualTo(charge2);
        charge1.setId(null);
        assertThat(charge1).isNotEqualTo(charge2);
    }
}
