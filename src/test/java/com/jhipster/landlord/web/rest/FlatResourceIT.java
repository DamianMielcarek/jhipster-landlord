package com.jhipster.landlord.web.rest;

import com.jhipster.landlord.domain.Flat;
import com.jhipster.landlord.repository.FlatRepository;
import com.jhipster.landlord.service.FlatService;
import com.jhipster.landlord.JhipsterLandlordApp;
import com.jhipster.landlord.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

import static com.jhipster.landlord.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link FlatResource} REST controller.
 */
@SpringBootTest(classes = JhipsterLandlordApp.class)
public class FlatResourceIT {

    private static final String DEFAULT_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS = "BBBBBBBBBB";

    private static final Integer DEFAULT_NUMBER_OF_ROOMS = 1;
    private static final Integer UPDATED_NUMBER_OF_ROOMS = 2;

    private static final Integer DEFAULT_FLOOR = 1;
    private static final Integer UPDATED_FLOOR = 2;

    @Autowired
    private FlatRepository flatRepository;

    @Mock
    private FlatRepository flatRepositoryMock;

    @Mock
    private FlatService flatServiceMock;

    @Autowired
    private FlatService flatService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restFlatMockMvc;

    private Flat flat;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final FlatResource flatResource = new FlatResource(flatService);
        this.restFlatMockMvc = MockMvcBuilders.standaloneSetup(flatResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Flat createEntity(EntityManager em) {
        Flat flat = new Flat()
            .address(DEFAULT_ADDRESS)
            .numberOfRooms(DEFAULT_NUMBER_OF_ROOMS)
            .floor(DEFAULT_FLOOR);
        return flat;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Flat createUpdatedEntity(EntityManager em) {
        Flat flat = new Flat()
            .address(UPDATED_ADDRESS)
            .numberOfRooms(UPDATED_NUMBER_OF_ROOMS)
            .floor(UPDATED_FLOOR);
        return flat;
    }

    @BeforeEach
    public void initTest() {
        flat = createEntity(em);
    }

    @Test
    @Transactional
    public void createFlat() throws Exception {
        int databaseSizeBeforeCreate = flatRepository.findAll().size();

        // Create the Flat
        restFlatMockMvc.perform(post("/api/flats")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(flat)))
            .andExpect(status().isCreated());

        // Validate the Flat in the database
        List<Flat> flatList = flatRepository.findAll();
        assertThat(flatList).hasSize(databaseSizeBeforeCreate + 1);
        Flat testFlat = flatList.get(flatList.size() - 1);
        assertThat(testFlat.getAddress()).isEqualTo(DEFAULT_ADDRESS);
        assertThat(testFlat.getNumberOfRooms()).isEqualTo(DEFAULT_NUMBER_OF_ROOMS);
        assertThat(testFlat.getFloor()).isEqualTo(DEFAULT_FLOOR);
    }

    @Test
    @Transactional
    public void createFlatWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = flatRepository.findAll().size();

        // Create the Flat with an existing ID
        flat.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restFlatMockMvc.perform(post("/api/flats")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(flat)))
            .andExpect(status().isBadRequest());

        // Validate the Flat in the database
        List<Flat> flatList = flatRepository.findAll();
        assertThat(flatList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkAddressIsRequired() throws Exception {
        int databaseSizeBeforeTest = flatRepository.findAll().size();
        // set the field null
        flat.setAddress(null);

        // Create the Flat, which fails.

        restFlatMockMvc.perform(post("/api/flats")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(flat)))
            .andExpect(status().isBadRequest());

        List<Flat> flatList = flatRepository.findAll();
        assertThat(flatList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNumberOfRoomsIsRequired() throws Exception {
        int databaseSizeBeforeTest = flatRepository.findAll().size();
        // set the field null
        flat.setNumberOfRooms(null);

        // Create the Flat, which fails.

        restFlatMockMvc.perform(post("/api/flats")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(flat)))
            .andExpect(status().isBadRequest());

        List<Flat> flatList = flatRepository.findAll();
        assertThat(flatList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkFloorIsRequired() throws Exception {
        int databaseSizeBeforeTest = flatRepository.findAll().size();
        // set the field null
        flat.setFloor(null);

        // Create the Flat, which fails.

        restFlatMockMvc.perform(post("/api/flats")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(flat)))
            .andExpect(status().isBadRequest());

        List<Flat> flatList = flatRepository.findAll();
        assertThat(flatList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllFlats() throws Exception {
        // Initialize the database
        flatRepository.saveAndFlush(flat);

        // Get all the flatList
        restFlatMockMvc.perform(get("/api/flats?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(flat.getId().intValue())))
            .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS.toString())))
            .andExpect(jsonPath("$.[*].numberOfRooms").value(hasItem(DEFAULT_NUMBER_OF_ROOMS)))
            .andExpect(jsonPath("$.[*].floor").value(hasItem(DEFAULT_FLOOR)));
    }
    
    @SuppressWarnings({"unchecked"})
    public void getAllFlatsWithEagerRelationshipsIsEnabled() throws Exception {
        FlatResource flatResource = new FlatResource(flatServiceMock);
        when(flatServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        MockMvc restFlatMockMvc = MockMvcBuilders.standaloneSetup(flatResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restFlatMockMvc.perform(get("/api/flats?eagerload=true"))
        .andExpect(status().isOk());

        verify(flatServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({"unchecked"})
    public void getAllFlatsWithEagerRelationshipsIsNotEnabled() throws Exception {
        FlatResource flatResource = new FlatResource(flatServiceMock);
            when(flatServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));
            MockMvc restFlatMockMvc = MockMvcBuilders.standaloneSetup(flatResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restFlatMockMvc.perform(get("/api/flats?eagerload=true"))
        .andExpect(status().isOk());

            verify(flatServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    @Transactional
    public void getFlat() throws Exception {
        // Initialize the database
        flatRepository.saveAndFlush(flat);

        // Get the flat
        restFlatMockMvc.perform(get("/api/flats/{id}", flat.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(flat.getId().intValue()))
            .andExpect(jsonPath("$.address").value(DEFAULT_ADDRESS.toString()))
            .andExpect(jsonPath("$.numberOfRooms").value(DEFAULT_NUMBER_OF_ROOMS))
            .andExpect(jsonPath("$.floor").value(DEFAULT_FLOOR));
    }

    @Test
    @Transactional
    public void getNonExistingFlat() throws Exception {
        // Get the flat
        restFlatMockMvc.perform(get("/api/flats/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateFlat() throws Exception {
        // Initialize the database
        flatService.save(flat);

        int databaseSizeBeforeUpdate = flatRepository.findAll().size();

        // Update the flat
        Flat updatedFlat = flatRepository.findById(flat.getId()).get();
        // Disconnect from session so that the updates on updatedFlat are not directly saved in db
        em.detach(updatedFlat);
        updatedFlat
            .address(UPDATED_ADDRESS)
            .numberOfRooms(UPDATED_NUMBER_OF_ROOMS)
            .floor(UPDATED_FLOOR);

        restFlatMockMvc.perform(put("/api/flats")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedFlat)))
            .andExpect(status().isOk());

        // Validate the Flat in the database
        List<Flat> flatList = flatRepository.findAll();
        assertThat(flatList).hasSize(databaseSizeBeforeUpdate);
        Flat testFlat = flatList.get(flatList.size() - 1);
        assertThat(testFlat.getAddress()).isEqualTo(UPDATED_ADDRESS);
        assertThat(testFlat.getNumberOfRooms()).isEqualTo(UPDATED_NUMBER_OF_ROOMS);
        assertThat(testFlat.getFloor()).isEqualTo(UPDATED_FLOOR);
    }

    @Test
    @Transactional
    public void updateNonExistingFlat() throws Exception {
        int databaseSizeBeforeUpdate = flatRepository.findAll().size();

        // Create the Flat

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFlatMockMvc.perform(put("/api/flats")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(flat)))
            .andExpect(status().isBadRequest());

        // Validate the Flat in the database
        List<Flat> flatList = flatRepository.findAll();
        assertThat(flatList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteFlat() throws Exception {
        // Initialize the database
        flatService.save(flat);

        int databaseSizeBeforeDelete = flatRepository.findAll().size();

        // Delete the flat
        restFlatMockMvc.perform(delete("/api/flats/{id}", flat.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<Flat> flatList = flatRepository.findAll();
        assertThat(flatList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Flat.class);
        Flat flat1 = new Flat();
        flat1.setId(1L);
        Flat flat2 = new Flat();
        flat2.setId(flat1.getId());
        assertThat(flat1).isEqualTo(flat2);
        flat2.setId(2L);
        assertThat(flat1).isNotEqualTo(flat2);
        flat1.setId(null);
        assertThat(flat1).isNotEqualTo(flat2);
    }
}
